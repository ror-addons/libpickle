<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="LibPickle" version="0.2.2" date="01/02/2009">
		<Author name="Bloodwalker" email="metagamegeek@gmail.com" />
		<Description text="Data Serialization Library (Pickling)" />
		<Dependencies/>
		<Files>
			<File name="LibStub.lua" />
			<File name="LibPickle.lua" />
		</Files>
		<OnInitialize/>
		<SavedVariables/>
	</UiMod>
</ModuleFile>